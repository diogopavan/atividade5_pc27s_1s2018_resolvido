/**
 * Exemplo de BufferSincronizado com métodos
 * wait() e notifyAll()
 * 
 * Autor: Lucio Agostinho Rocha
 * Ultima modificacao: 19/10/2017
 */
package atividade5;

/**
 *
 * @author Lucio
 */
public class BufferSincronizado implements Buffer {

    private int buffer = -1; // compartilhado por produtor e consumidor
    private boolean temValor = false; //indica se o buffer temValor
    
    //Insere o valor no buffer
    public synchronized void set(int valor) throws InterruptedException{
        
        //Enquanto nao houver posicoes vazias, coloca a thread em estado de Espera
        while ( temValor ){
//          System.out.println("Produtor tenta escrever. Buffer cheio. Produtor espera. | Buffer: " + buffer + " temValor: " + temValor);
//            System.out.println("Ping");
            wait();
        }
        
        buffer = valor; //insere um valor no buffer
        
        //Indica que o produtor não pode armazenar outro valor ate que
        //o consumidor recupere o valor do buffer.
        temValor = true; 
        
//        System.out.println("Produtor escreve: " + buffer+ " | Buffer: " + buffer + " temValor: " + temValor);
        System.out.println("Ping");
        //Informa a todas as outras threads em espera para
        //entrarem no estado Executável
        notifyAll(); 
    }
    
    public synchronized int get() throws InterruptedException{
    
        while (!temValor){
//            System.out.println("Consumidor tenta ler. Buffer vazio. Consumidor espera. | Buffer: " + buffer + " temValor: " + temValor);
//            System.out.println("Ping");
            wait();
        }
        
        //Consumidor acabou de recuperar valor do buffer.
        //Produtor pode inserir outro valor
        temValor=false;
        
//        System.out.println("Consumidor lê: " + buffer + " | Buffer: " + buffer + " temValor: " + temValor);
        System.out.println("Pong");
        //Informa a todas as outras threads em espera para
        //entrarem no estado Executável
        notifyAll();
        
        return buffer;
        
    }//fim get
    
}//fim classe
